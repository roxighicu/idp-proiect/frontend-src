import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { NAVIGATION_PATHS } from "./navigation/navigation.constants";
import WelcomePage from "./pages/welcome-page/WelcomePage";
import { Typography } from "@mui/material";
// atat
const App = () => {
  return (
    <Router>
      <div className="App">
        <nav className={"top-navigation-container"}>
          <Typography variant={"h2"} style={{ marginLeft: 75, color: "white" }}>
            {"Logo"}
          </Typography>
        </nav>
        <Routes>
          <Route path={NAVIGATION_PATHS.welcome} element={<WelcomePage />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
