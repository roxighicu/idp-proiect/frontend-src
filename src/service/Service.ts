const Api = require("axios").default;
export default Api;

export const addNewPost = ({
  title,
  comment,
}: {
  title: string;
  comment: string;
}): Promise<string> => {
  return Api.post(
    "http://localhost/api/new",
    JSON.stringify({
      title: title,
      comment: comment,
    })
  );
};

export const getPosts = (): Promise<string> => {
  return Api.get("http://localhost/api/posts");
};
