import React, { useEffect, useState } from "react";
import { Card, CardContent, Typography } from "@mui/material";
import * as Service from "../../service/Service";

const WelcomePage = () => {
  const [commentInput, setCommentInput] = useState<string>("");
  const [titleInput, setTitleInput] = useState<string>("");

  const [posts, setPosts] = useState<{ title: String; comment: String }[]>([]);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    Service.addNewPost({
      title: titleInput,
      comment: commentInput,
    })
      .then((response: any) => {
        console.log("Success: " + response);
        fetchPosts();
        setCommentInput("");
        setTitleInput("");
      })
      .catch((error) => {
        console.log("Error: " + error);
      });
  };

  const fetchPosts = () => {
    Service.getPosts().then((r) => {
      setPosts((r as any).data.reverse() as any);
    });
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div
      style={{ justifyContent: "flex-start", marginTop: 100 }}
      className="App"
    >
      <form onSubmit={handleSubmit}>
        <div
          className="input-container"
          style={{
            marginTop: 16,
          }}
        >
          <Typography variant={"h6"}>{"Share anything here:"}</Typography>
          <input
            type="text"
            name="username"
            required
            style={{
              borderRadius: 20,
              borderWidth: 1,
              paddingLeft: 16,
              height: 24,
              width: 800,
            }}
            onChange={(event) => setTitleInput(event.target.value)}
          />
        </div>
        <div
          className="input-container"
          style={{
            marginTop: 16,
          }}
        >
          <input
            type="text"
            name="username"
            required
            style={{
              borderRadius: 20,
              borderWidth: 1,
              paddingLeft: 16,
              height: 100,
              width: 800,
            }}
            onChange={(event) => setCommentInput(event.target.value)}
          />
        </div>
        <div className="button-container">
          <input type="submit" style={{ borderRadius: 20, marginTop: 15 }} />
        </div>
      </form>

      <Typography sx={{ fontSize: 36, marginTop: 5 }} variant={"h2"}>
        {"New posts"}
      </Typography>

      {posts.map((post) => {
        return (
          <Card sx={{ width: 800, marginTop: 5 }}>
            <CardContent>
              <Typography sx={{ fontSize: 14, fontWeight: 900 }} gutterBottom>
                {post.title}
              </Typography>
              <Typography variant="body2">{post.comment}</Typography>
            </CardContent>
          </Card>
        );
      })}
    </div>
  );
};

export default WelcomePage;
