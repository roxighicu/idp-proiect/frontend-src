FROM node:14.17.0-alpine
WORKDIR /app
COPY package.json /app/
RUN yarn
RUN yarn add react-scripts
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]